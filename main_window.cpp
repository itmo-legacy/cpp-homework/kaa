#include "main_window.hpp"
#include "ui_main_window.h"

#include <QCommonStyle>
#include <QDesktopWidget>
#include <QDir>
#include <QFileDialog>
#include <QFileInfo>
#include <QMessageBox>
#include <QtConcurrent>
#include <QtGui/QDesktopServices>
#include <QtWidgets/QProgressDialog>
#include <future>

namespace fs = std::filesystem;

main_window::main_window(QWidget *parent)
    : QMainWindow(parent), progress_dialog(this), ui(new Ui::MainWindow) {
    qRegisterMetaType<state>("state");
    qRegisterMetaType<std::string>("std::string");
    qRegisterMetaType<std::vector<kaa::file_index::entry>>("std::vector<kaa::file_index::entry>");

    ui->setupUi(this);
    setGeometry(QStyle::alignedRect(Qt::LeftToRight, Qt::AlignCenter, size(), qApp->desktop()->availableGeometry()));

    connect(ui->lineEdit, &QLineEdit::textChanged, this, &main_window::state_searching);

    ui->treeWidget->header()->setSectionResizeMode(0, QHeaderView::Stretch);
    ui->treeWidget->header()->hide();

    connect(ui->treeWidget, &QTreeWidget::itemDoubleClicked, [](QTreeWidgetItem *item) {
        if (item->parent()) {
            QDesktopServices::openUrl(QUrl::fromLocalFile(item->parent()->text(0)));
        }
    });

    connect(this, &main_window::gathered_files, [this](size_t count) {
        progress_dialog.setMaximum(int(count));
        progress_dialog.setLabelText(QString("Gathering files… %1 files found").arg(count));
    });
    connect(&progress_dialog, &QProgressDialog::canceled, this, &main_window::state_cancelled);
    progress_dialog.setWindowTitle("Please, Wait…");
    progress_dialog.setWindowModality(Qt::WindowModal);
    progress_dialog.reset(); // so that it didn't appear 2 seconds after start for no reason, TODO sane fix

    QCommonStyle style;
    ui->actionScan_Directory->setIcon(style.standardIcon(QCommonStyle::SP_DialogOpenButton));
    ui->actionExit->setIcon(style.standardIcon(QCommonStyle::SP_DialogCloseButton));
    ui->actionAbout->setIcon(style.standardIcon(QCommonStyle::SP_DialogHelpButton));

    connect(ui->actionScan_Directory, &QAction::triggered, this, &main_window::select_directory);
    connect(ui->actionExit, &QAction::triggered, this, &QWidget::close);
    connect(ui->actionAbout, &QAction::triggered, this, &main_window::show_about_dialog);

    connect(this, &main_window::operation_cancelled, this, &main_window::state_cancelled);
    connect(this, &main_window::error_encountered, this, &main_window::state_error);
    connect(this, &main_window::gathering_finished, this, &main_window::state_indexing);

    connect(&indexing_watcher, &QFutureWatcher<void>::progressValueChanged, &progress_dialog, &QProgressDialog::setValue);
    connect(&indexing_watcher, &QFutureWatcher<void>::finished, [this] {
        entries.clear();
        if (not indexing_watcher.isCanceled()) {
            state_indexing_finished();
        }
    });

    connect(this, &main_window::found_in_file, this, &main_window::display_found);
    connect(&searching_watcher, &QFutureWatcher<void>::finished, this, &main_window::state_searching_finished);

    connect(&filesystem_watcher, &QFileSystemWatcher::fileChanged,
            [this](QString file) {
                auto it = indices.find(file.toStdString());
                if (it == indices.end()) {
                    qWarning("filesystem_watcher registered changes of a file, absent in indices: '%s'", qUtf8Printable(file));
                } else {
                    it->second.update(cancelled);
                }
            });

    clear();
    ui->statusBar->setText("");
    idle.store(true, std::memory_order_relaxed);
}

main_window::~main_window() {
    cancelled.store(true, std::memory_order_relaxed);
    cancel();
    indexing_watcher.waitForFinished();
    searching_watcher.waitForFinished();
}

void main_window::show_about_dialog() {
    QMessageBox::aboutQt(this);
}

void main_window::clear() {
    ui->lineEdit->setDisabled(true);
    ui->lineEdit->clearFocus();
    ui->treeWidget->clear();
}

void main_window::cancel() {
    indexing_watcher.cancel();
    searching_watcher.cancel();
}

void main_window::select_directory() {
    cancel();
    if (not idle.load(std::memory_order_relaxed)) {
        QThread::msleep(100);
    }

    if (not idle.load(std::memory_order_relaxed)) {
        QMessageBox::information(this, "Please, Wait", "It might take time to cancel current opration. Wait until then.");
    } else {
        QString dir = QFileDialog::getExistingDirectory(this, "Select Directory for Indexing", QString(),
                                                        QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks);
        if (dir == "") {
            return;
        }
        state_gathering(dir);
    }
}

void main_window::state_cancelled() {
    cancel();
    clear();
    ui->statusBar->setText("Cancelled");
    cancelled.store(true, std::memory_order_relaxed);
    idle.store(true, std::memory_order_relaxed);
}

void main_window::state_error() {
    clear();
    ui->statusBar->setText("Couldn't complete the operation");
    idle.store(true, std::memory_order_relaxed);
}

void main_window::state_gathering(const QString &dir) {
    if (not idle.load(std::memory_order_relaxed)) {
        QMessageBox::information(this, "Nope", "Can't do that. There are operations running.");
        return;
    }

    entries.clear();
    clear();
    progress_dialog.show();
    ui->statusBar->setText("Gathering files…");
    cancelled.store(false, std::memory_order_relaxed);
    idle.store(false, std::memory_order_relaxed);

    setWindowTitle(QString("Indexed Directory - %1").arg(dir));

    QtConcurrent::run([this, dir]() {
        std::error_code ec;
        fs::path path(dir.toStdString());
        if (fs::is_symlink(path, ec)) {
            path = fs::read_symlink(path, ec);
        }
        if (path.filename() == ".") {
            path.remove_filename();
        }

        if (not fs::is_directory(path, ec)) {
            qWarning("'%s' is not a directory", path.c_str());
            emit error_encountered();
            return;
        }

        auto seq = fs::recursive_directory_iterator(path, ec);
        for (auto it = begin(seq); it != end(seq); it.increment(ec)) {
            if (entries.size() % 1024 == 0) {
                if (cancelled.load(std::memory_order_relaxed)) {
                    emit operation_cancelled();
                    return;
                }
                emit gathered_files(entries.size());
            }
            entries.emplace_back(*it);
        }

        emit gathering_finished();
    });
}

void main_window::state_indexing() {
    cancel();
    indexing_timer.start();
    filesystem_watcher.removePaths(filesystem_watcher.files());
    progress_dialog.setLabelText(QString("Found %1 files, now processing…").arg(entries.size()));
    ui->statusBar->setText("Indexing…");
    cancelled.store(false, std::memory_order_relaxed);
    idle.store(false, std::memory_order_relaxed);

    indexing_watcher.setFuture(
        QtConcurrent::map(entries, [this](auto const &entry) {
            kaa::file_index index(entry, cancelled);
            if (not index.empty()) {
                std::scoped_lock lock(mutex);
                indices.try_emplace(entry.path().u8string(), std::move(index));
                bool ok = filesystem_watcher.addPath(entry.path().c_str());
                if (not ok) {
                    qWarning("couldn't add '%s'", entry.path().c_str());
                }
            }
        }));
}

void main_window::state_indexing_finished() {
    qInfo("indexing finished in %.3fs.", indexing_timer.elapsed() / 1000.0);
    progress_dialog.hide();
    ui->lineEdit->setEnabled(true);
    ui->statusBar->setText("Indexing finished");
    idle.store(true, std::memory_order_relaxed);
}

void main_window::state_searching(QString const &text) {
    if (text.size() > 2) {
        cancel();
        searching_timer.start();
        ui->treeWidget->clear();
        ui->statusBar->setText("Searching…");
        cancelled.store(false, std::memory_order_relaxed);
        idle.store(false, std::memory_order_relaxed);

        needle   = text.toStdString();
        searcher = std::boyer_moore_horspool_searcher{needle.begin(), needle.end()};
        searching_watcher.setFuture(
            QtConcurrent::map(indices, [this](auto const &entry) {
                auto const &[path, index] = entry;
                auto found                = index.lookup(needle, searcher, cancelled);
                emit found_in_file(path, std::move(found));
            }));
    }
}

void main_window::state_searching_finished() {
    qInfo("searching finished in %.3fs.", searching_timer.elapsed() / 1000.0);
    ui->statusBar->setText("Searching finished");
    idle.store(true, std::memory_order_relaxed);
}

void main_window::display_found(std::string const &filename, std::vector<kaa::file_index::entry> const &entries) {
    if (entries.empty()) {
        return;
    }

    auto *tree_item = new QTreeWidgetItem(ui->treeWidget);
    tree_item->setText(0, filename.c_str());
    for (auto &&entry : entries) {
        auto *entry_item = new QTreeWidgetItem(tree_item);
        entry_item->setText(0, QString("line: %1, symbol: %2").arg(entry.line_no + 1).arg(entry.symbol_no + 1));
    }
}
