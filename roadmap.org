* Resources
** hashsets
*** google [[https://github.com/abseil/abseil-cpp/tree/master/absl/container][absl::flat_hash_set]]
*** [[https://github.com/martinus/robin-hood-hashing][robin-hood hashing]]
** [[https://github.com/google/codesearch][trigrams]]
*** consider ignoring files that:
    - [X] have lines longer than 2000 (?) symbols
    - [X] are longer than 2^30 symbols
    - [X] have more than 20'000 trigrams
    - [X] are not valid utf-8
      - [X] check for some symbols that can't appear in utf-8
      - [X] check for else
* Short-term plans
** DONE maybe merge gui and logic
   CLOSED: [2019-01-05 Sat 20:46]
   Right now, logic (~directory_index~) is too dependent on gui.
   Done in a separate branch.
** TODO add directory watching
* Far-going plans
** DONE recursive file system watching
   CLOSED: [2019-01-13 Sun 15:10]
   inotify on linux, … on others
** DONE cache indices
   CLOSED: [2019-01-13 Sun 15:10]
** TODO show line with highlighted words in result previews
   - delegates
   - too slow to do simultaneously for all items
** TODO use std algorithms with execution policies instead of QtConcurrent
   Not implemented in libstdc++ right now
** TODO search in symlinked folders as well
** TODO ignore case option
** TODO ignore number of whitespaces
** DONE multithreaded indexing
   CLOSED: [2019-01-03 Thu 20:52]
* Google when I get to the Internet
** DONE does SAR allow aliasing with unsigned char *?
   It does. As a matter of fact, any character type might alias.
